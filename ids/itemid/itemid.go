package itemid

// ItemType is the unique identifier for each type of Item in the game
type ItemType int

// The IDs for all items in the game
const (
	None ItemType = 0
	Coal
)
