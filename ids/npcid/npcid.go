package npcid

// NPCType is the unique identifier for each type of NPC in the game
type NPCType int

// The IDs for all npcs in the game
const (
	None NPCType = 0
	Goblin
)
