package roomid

import "strconv"

// RoomType is a constant representing a specific room.
type RoomType int

// Room identifier constants
const (
	NoExit            RoomType = 0
	WhitehallFountain          = 1
	LoginPassword              = 2
	LoginUsername              = 3
	NewUser                    = 4
	NewCharName                = 5
	NewCharRace                = 6
	NewCharClass               = 7
	NewCharGender              = 8
	NewCharStats               = 9
)

// AtoRT converts a numeric string to its matching RoomType enum
func AtoRT(s string) RoomType {
	i, err := strconv.Atoi(s)
	if err != nil {
		return NoExit
	}
	return RoomType(i)
}
