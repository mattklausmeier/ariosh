package factory

import (
	"fmt"

	"bitbucket.org/mattklausmeier/ariosh/ids/itemid"
	"bitbucket.org/mattklausmeier/ariosh/ids/npcid"
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/items"
	"bitbucket.org/mattklausmeier/ariosh/npcs"
	"bitbucket.org/mattklausmeier/ariosh/player"

	"bitbucket.org/mattklausmeier/ariosh/intf"
	"bitbucket.org/mattklausmeier/ariosh/rooms"
)

// Factory is the object used to create rooms, players, and items of specified types
type Factory struct {
}

// Room returns a new Room object of the specified type
func (f *Factory) Room(room roomid.RoomType) intf.Room {
	switch room {
	case roomid.WhitehallFountain:
		rm := rooms.RoomWhitehallFountain{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.WhitehallFountain}}
		rm.Poke(nil)
		return &rm
	case roomid.LoginPassword:
		rm := rooms.RoomLoginPassword{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.LoginPassword}}
		rm.Poke(nil)
		return &rm
	case roomid.LoginUsername:
		rm := rooms.RoomLoginUsername{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.LoginUsername}}
		rm.Poke(nil)
		return &rm
	case roomid.NewUser:
		rm := rooms.RoomNewUser{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.NewUser}}
		rm.Poke(nil)
		return &rm
	case roomid.NewCharName:
		rm := rooms.RoomNewCharName{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.NewCharName}}
		rm.Poke(nil)
		return &rm
	case roomid.NewCharRace:
		rm := rooms.RoomNewCharRace{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.NewCharRace}}
		rm.Poke(nil)
		return &rm
	case roomid.NewCharClass:
		rm := rooms.RoomNewCharClass{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.NewCharClass}}
		rm.Poke(nil)
		return &rm
	case roomid.NewCharGender:
		rm := rooms.RoomNewCharGender{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.NewCharGender}}
		rm.Poke(nil)
		return &rm
	case roomid.NewCharStats:
		rm := rooms.RoomNewCharStats{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.NewCharStats}}
		rm.Poke(nil)
		return &rm
	default:
		fmt.Println("!!! ERROR: Unimplemented room type in factory/factory.go/Room(): ", room)
		rm := rooms.RoomWhitehallFountain{BaseRoom: rooms.BaseRoom{Factory: f, RoomId: roomid.WhitehallFountain}}
		rm.Poke(nil)
		return &rm
	}
}

// Player returns a new Player object
func (f *Factory) Player() intf.Player {
	p := player.Player{}
	p.Poke()
	return &p
}

// Item returns a new Item object of the specified type
func (f *Factory) Item(i itemid.ItemType) intf.Item {
	switch i {
	case itemid.Coal:
		return &items.Coal{}
	default:
		fmt.Println("!!! ERROR: Unimplemented item type in factory/factory.go/Item(): ", i)
		return &items.BaseItem{}
	}
}

// NPC returns a new instance of the specified type
func (f *Factory) NPC(t npcid.NPCType) intf.NPC {
	switch t {
	case npcid.Goblin:
		n := npcs.Goblin{}
		n.Poke(n.Heartbeat)
		return &n
	default:
		fmt.Println("!!! ERROR: Unimplemented NPC type in factory/factory.go/NPC(): ", t)
		return &npcs.BaseNPC{}
	}
}
