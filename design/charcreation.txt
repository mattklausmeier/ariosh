Choose Your Character's Race:
"Late at night, as you calm your mind in preparation for sleep, you can still recall the sensation of being lovingly cradled in your father's arms.  What do your tiny hands feel as they reach out for his face?"
1) A mighty, braided beard and a thick nose. (Dwarf or Half-Dwarf)
2) Long, pointed ears and smooth chin. (Elf or Half-Elf)
3) Greenish skin and fierceome tusks. (Orc or Half-Orc)
4) Scaley skin and eyes with pupil slits. (Drakkin)
5) Round, pink ears and a scratchy beard. (Human)
6) Fur and whiskers? (Fokhai)

(If 1-3...)
"Did the other children tease you for [being tall/having pink skin/having rounded ears]?"
1) Yes, my mother was a human.
2) No, I'm pure [race], through and through!

Choose Your Character's Class:
"When you first attempted to mimic your father in his profession, you were good for little more than a laugh.  But he could barely contain his pride on the day you sucessfully acommplished what?"
1) Riding out with the city watch to track down some maurading goblins. (Fighter)
2) Conjuring the flame to ignite the winter festival bonfire. (Wizard)
3) Healing your best friend's arm after he fell out of a tree. (Cleric)
4) Picking the door to the cell where your father was being held prisoner. (Thief)

Choose Your Character's Gender:
"At least once a year, your mother recounts the story (much to your father's chagrin) of the friendly wager they made before knowing if they were having a boy or a girl.  Your mother got a month's worth of foot massages by correctly guessing that you would be:"
1) A boy.
2) A girl.

Choose Your Character's Stats:
When you interact with the world, you do so using a custom variety of skills and strengths.  Those skills and strengths are described using the following 6 stats:
  Strength - Your ability to apply physical force.
  Dexterity - Your deftness, nimbleness, and fine-motor control.
  Constitution - How tough and resiliant you are.
  Charisma - Your charming way with people and animals.
  Magic - Your ability to bend the mysterious forces of the universe to your will.
  Luck - How much the universe tilts the odds in your favor.
Each of your 6 attributes starts at 10.  Each of the following 6 questions will allow you to raise an attribute by 2 points.  Each attribute can be raised no more than 4 points.  (For example, you could put two points into each stat, or put four points into just three of your stats, leaving the rest at 10.)

#1 "You think back to your first real fight: a childhood bully cornered you in an alley.  As the large boy made known his intentions towards violence, how did you respond?"
1) I threw the first punchand scared him off. (+2 Strength)
2) I nimbly dodged his blows until he was too tired to continue. (+2 Dexterity)
3) I took his punches, one after the other, until he realized his efforts were in vain. (+2 Constitution)
4) I used my charming wit to try to soothe the hostile beast. (+2 Charisma)
5) I caused a burst of light from my fingers, blinding him long enough to escape. (+2 Magic)
6) I called out to a passing guard who happened to be walking by. (+2 Luck)

#2 "Each spring, your village has a festival with many sports and activities.  What do you do to enjoy yourself?"
1) Compete in the arm wrestling competition. (+2 Strength)
2) Practice my pick-pocketing among the distracted crowds. (+2 Dexterity)
3) Drink all challengers under the table. (+2 Constitution)
4) Anything, so long as you bring a date. (+2 Charisma)
5) Learn new spells from the traveling booksellers. (+2 Magic)
6) Win some coin at cards. (+2 Luck)

#3 "The saddest day of your childhood was watching helpless as your dog drowned in the river.  On that day you regretted:"
1) Not being a stronger swimmer. (+2 Strength)
2) Not being able to throw a lasso. (+2 Dexterity)
3) Not being able to hold your breath longer. (+2 Constitution)
4) Not having anyone to help. (+2 Charisma)
5) Not knowing a spell useful to the situation. (+2 Magic)
6) Having such rotten luck. (+2 Luck)

#4 "A horse-drawn wagon is out of control, and there's a terrified child on board.  How do you save her?"
1) Grab the reigns and control the beast. (+2 Strength)
2) Leap into the wagon and lift the child to safety. (+2 Dexterity)
3) Step in front of the wagon, blocking its path with your body. (+2 Constitution)
4) Calm the horse with your soothing words. (+2 Charisma)
5) Casting a calming spell upon the beast. (+2 Magic)
6) As you're about to act, the horse breaks free and the wagon rolls to a stop. (+2 Luck)

#5 "When you were younger, your mother would call you into the kitchen for what reason?"
1) To lift something heavy. (+2 Strength)
2) To reach something up high. (+2 Dexterity)
3) To see if the milk had gone bad.d (+2 Constitution)
4) To keep her company. (+2 Charisma)
5) To kindle the stove fire. (+2 Magic)
6) To find something tiny she had dropped. (+2 Luck)

#6 "Where do you hide your mooney?"
1) Under a heavy stone. (+2 Strength)
2) Up in the rafters. (+2 Dexterity)
3) Beneath the garbage pile. (+2 Constitution)
4) People don't charge you when you're this handsome! (+2 Charisma)
5) In a box with no lid. (+2 Magic)
6) You just see to always find what you need. (+2 Luck)

Choose Your Character's Name:
"It's time to choose your character's name.  Here are the guidelines:
[hr]
Must be at least 2 characters long
Must be no more than 20 characters long
Can only contain letters, spaces, and punctuation
Valid punctuation: period, comma, dash, single quote, double quote
Leading/trailing spaces will be ignored
Must not be the name of an existing character
No double spaces
Should be something fantasy sounding and not easily recognizable (No "Trump" or "Frodo")
Must not be confusingly similar to another player's name.
