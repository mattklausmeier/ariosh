package items

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/itemid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// Coal is a standard, earth-style lump of coal
type Coal struct {
	BaseItem
}

// GetDescription returns the
func (c *Coal) GetDescription(dt intf.DescriptionType) string {
	switch dt {
	case intf.BriefDescription:
		return "a lump of coal"
	case intf.LookDescription:
		return "a rough, half-pound lump of coal"
	}
	return "coal"
}

// ItemID returns the item id for this item
func (c *Coal) ItemID() itemid.ItemType {
	return itemid.Coal
}
