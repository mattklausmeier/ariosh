package items

import (
	"strconv"

	"bitbucket.org/mattklausmeier/ariosh/ids/itemid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// BaseItem is a helper class that simple items can inherit from in order
// to reuse code.  Specifically, the marshalling functions can be inherited
// if the item has no data to marshall/unmarshall
type BaseItem struct {
}

// Marshall returns the string representation of an item for saving to disk
// The string representation is in the format itemId{itemData}
// itemData can be any string, so long as it doesn't have a closing curly brace;
// the item is in charge of parsing and using that data
func (c *BaseItem) Marshall(i itemid.ItemType) string {
	return strconv.Itoa(int(i)) + "{}"
}

// Unmarshall initializes an item with its marshalled values
func (c *BaseItem) Unmarshall(s string) {

}

// ItemID returns the item id for this item
func (c *BaseItem) ItemID() itemid.ItemType {
	return itemid.None
}

// GetDescription returns the description of the item
func (c *BaseItem) GetDescription(d intf.DescriptionType) string {
	return "Unimplemented Item"
}
