package npcs

import (
	"time"
)

// BaseNPC represents any server creature in the game.
type BaseNPC struct {
	poked bool
}

type heartbeat func()

// Poke starts this NPCs heartbeat
func (n *BaseNPC) Poke(f heartbeat) {
	if f == nil {
		return
	}
	if !n.poked {
		n.poked = true
		ticker := time.NewTicker(1 * time.Second)
		go func() {
			for range ticker.C {
				f()
			}
		}()
	}
}

// Name returns the NPC's name
func (n *BaseNPC) Name() string {
	return "Unnamed npc"
}
