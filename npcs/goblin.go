package npcs

// Goblin is the goblin class
type Goblin struct {
	BaseNPC
}

// Heartbeat is called at regular inter
func (g *Goblin) Heartbeat() {
}

// Name returns the name of the Goblin
func (g *Goblin) Name() string {
	return "Goblin"
}
