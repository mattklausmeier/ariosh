package player

import (
	"strings"
	"time"

	"bitbucket.org/mattklausmeier/ariosh/cmd"
	"bitbucket.org/mattklausmeier/ariosh/intf"
	"golang.org/x/net/websocket"
)

// Player represents a user's online presense.
type Player struct {
	Game     intf.PlayerHandler
	conn     *websocket.Conn
	sendFail bool
	Room     intf.Room
	LoggedIn bool
	Username string
	Char     intf.Char
	inv      []intf.Item
	poked    bool
}

// SetRoom sets this player's room to the specified room
func (p *Player) SetRoom(r intf.Room) {
	p.Room = r
}

// SendToPlayer sends the specified string to this player.
func (p *Player) SendToPlayer(str ...string) {
	for _, s := range str {
		var err error
		if err = websocket.Message.Send(p.conn, s); err != nil {
			p.sendFail = true
			return
		}
	}
}

// DidSendFail is called when the game fails to send a message to this player.
func (p *Player) DidSendFail() bool {
	return p.sendFail
}

// HandlePlayerInput handles the input this player sends to the game.
func (p *Player) HandlePlayerInput(str string) {
	cmds := strings.SplitN(strings.TrimSpace(str), " ", 2)
	for _, v := range []string{"[pass]", "[header]", "[center]", "[title]", "[hr]", "[inv]"} {
		if strings.Index(str, v) >= 0 {
			return
		}
	}
	if !p.LoggedIn {
		p.GetRoom().HandleInput(p, str)
	} else {
		switch {
		case cmds[0] == "down" || cmds[0] == "d":
			cmd.Down(p)
		case cmds[0] == "east" || cmds[0] == "e":
			cmd.East(p)
		case cmds[0] == "help":
			cmd.Help(p)
		case cmds[0] == "listusers":
			cmd.ListUsers(p)
		case cmds[0] == "north" || cmds[0] == "n":
			cmd.North(p)
		case cmds[0] == "say":
			cmd.Say(p, str)
		case cmds[0] == "south" || cmds[0] == "s":
			cmd.South(p)
		case cmds[0] == "up" || cmds[0] == "u":
			cmd.Up(p)
		case cmds[0] == "west" || cmds[0] == "w":
			cmd.West(p)
		case cmds[0] == "who":
			cmd.Who(p)
		case cmds[0] == "i" || cmds[0] == "inv" || cmds[0] == "inventory":
			p.GetChar().Inv(p)
		case cmds[0] == "drop":
			if len(cmds) < 2 {
				p.SendToPlayer("Drop what?")
				return
			}
			cmd.Drop(p, cmds[1])
		case cmds[0] == "take":
			if len(cmds) < 2 {
				p.SendToPlayer("Take what?")
				return
			}
			cmd.Take(p, cmds[1])
		case cmds[0] == "look":
			cmd.Look(p)
		default:
			if !p.Room.HandleInput(p, str) {
				p.SendToPlayer("What?")
			}
		}
	}
}

// GetRoom returns a reference to the room the player is in
func (p *Player) GetRoom() intf.Room {
	return p.Room
}

// SetGame gives the player a reference to the main Game object
func (p *Player) SetGame(g intf.PlayerHandler) {
	p.Game = g
}

// GetGame returns the player's reference to the main Game object
func (p *Player) GetGame() intf.PlayerHandler {
	return p.Game
}

// SetChar sets the reference to the player's active character
func (p *Player) SetChar(c intf.Char) {
	p.Char = c
}

// GetChar returns the player's active character
func (p *Player) GetChar() *intf.Char {
	return &p.Char
}

// SetUsername sets the Username this player is logged in using
func (p *Player) SetUsername(s string) {
	p.Username = s
}

// GetUsername returns the username the player is logged in using
func (p *Player) GetUsername() string {
	return p.Username
}

// SetLoggedIn sets a flag representing whether the user is logged in
func (p *Player) SetLoggedIn(b bool) {
	p.LoggedIn = b
}

// GetLoggedIn returns a flag representing whether the user is logged in
func (p *Player) GetLoggedIn() bool {
	return p.LoggedIn
}

// SetConn sets a reference to this player's websocket connection
func (p *Player) SetConn(ws *websocket.Conn) {
	p.conn = ws
}

// GetConn returns the reference to this player's websocket connection
func (p *Player) GetConn() *websocket.Conn {
	return p.conn
}

// Poke starts the player's heartbeat
func (p *Player) Poke() {
	if !p.poked {
		p.poked = true
		ticker := time.NewTicker(1 * time.Second)
		go func() {
			for range ticker.C {
				p.Heartbeat()
			}
		}()
	}
}

// Heartbeat is called once per second
func (p *Player) Heartbeat() {
}
