package game

import (
	"time"

	"bitbucket.org/mattklausmeier/ariosh/dataio"
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
	"golang.org/x/net/websocket"
)

// Game is the main object that oversees the entire game operation.
type Game struct {
	players []intf.Player
	roomarr map[roomid.RoomType]intf.Room
	Factory intf.Factory
	poked   bool
}

// AddPlayer adds a player to the game's list of online players,
// and then puts them in the starting room.
func (g *Game) AddPlayer(ws *websocket.Conn) intf.Player {
	newPlayer := g.Factory.Player()
	newPlayer.SetGame(g)
	newPlayer.SetConn(ws)
	g.players = append(g.players, newPlayer)
	if g.roomarr == nil {
		g.roomarr = make(map[roomid.RoomType]intf.Room)
	}
	g.MovePlayerToRoom(newPlayer, roomid.LoginUsername)
	return newPlayer
}

// SaveChar saves a player's character
func (g *Game) SaveChar(p intf.Player) {
	if p == nil || p.GetChar() == nil || p.GetRoom() == nil {
		return
	}
	dataio.SaveChar(p.GetChar().Name, p.GetChar().GetRaceString(), p.GetChar().GetClassString(), p.GetChar().GetGenderString(), p.GetChar().GetStatString(), p.GetChar().MarshallInventory(), p.GetRoom().RoomIdString())
}

// RemovePlayer removes a player from the game's list of online players.
func (g *Game) RemovePlayer(p intf.Player) {
	if p == nil || p.GetRoom() == nil {
		return
	}
	g.SaveChar(p)
	p.GetRoom().LosePlayer(p)
	g.players = intf.RemovePlayerFromSlice(p, g.players)
}

// LoggedInPlayerCount returns the number of loged in players.
func (g *Game) LoggedInPlayerCount() int {
	count := 0
	for a := 0; a < len(g.players); a++ {
		if g.players[a] == nil {
			continue
		}
		if g.players[a].GetLoggedIn() {
			count++
		}
	}
	return count
}

// PlayerIsOnline returns whether a player with the specified username is online.
func (g *Game) PlayerIsOnline(str string) bool {
	for a := 0; a < len(g.players); a++ {
		if g.players[a].GetUsername() == str {
			if g.players[a] == nil {
				continue
			}
			return true
		}
	}
	return false
}

// MovePlayerToRoom moves the specified player to the specified room.
func (g *Game) MovePlayerToRoom(p intf.Player, room roomid.RoomType) {
	if p == nil {
		return
	}
	if _, ok := g.roomarr[room]; !ok {
		temp := g.Factory.Room(room)
		g.roomarr[room] = temp
	}
	if p.GetRoom() != nil {
		p.GetRoom().LosePlayer(p)
	}
	g.roomarr[room].TakePlayer(p)
}

// CrashBackup safely removes each player from the game, backing up their
// characters, in the event of a Ctrl+C
func (g *Game) CrashBackup() {
	for _, p := range g.players {
		if p == nil {
			return
		}
		p.SendToPlayer("Server Shutdown by Admin - Saving your character...")
		g.RemovePlayer(p)
	}
}

// Poke starts the room's heartbeat
func (g *Game) Poke() {
	if !g.poked {
		g.poked = true
		ticker := time.NewTicker(5 * time.Second)
		go func() {
			for range ticker.C {
				g.Heartbeat()
			}
		}()
	}
}

// Heartbeat is called every 5 seconds
func (g *Game) Heartbeat() {
	for k, v := range g.roomarr {
		if v == nil || v.ShouldDelete() {
			delete(g.roomarr, k)
		}
	}
}
