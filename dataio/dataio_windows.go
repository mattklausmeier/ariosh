package dataio

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func init() {
	path := ExePath()
	buildDataPath()
	_, err := ioutil.ReadDir(path + "\\data\\chars\\")
	if err != nil {
		os.Mkdir(path+"\\data\\chars\\", 0770)
	}
	_, err = ioutil.ReadDir(path + "\\data\\users\\")
	if err != nil {
		os.Mkdir(path+"\\data\\users\\", 0770)
	}
}

// ExePath returns the path of the directory where the exe resides on Windows
// This should be where the data folder lives
func ExePath() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal("Missing GOPATH; terminating ariosh process!")
		return ""
	}
	return dir
}

// PathSeparator returns the path separator character for Windows
func PathSeparator() string {
	return "\\"
}

// UserDir returns the path of the named user's user directory
func UserDir(usr string) string {
	sha := Hash(strings.ToLower(usr))
	return UsersDir() + sha
}

// CharDir returns the path of the named character's character directory
func CharDir(char string) string {
	sha := Hash(strings.ToLower(char))
	return CharsDir() + sha
}

// CharDataFile returns the file where the character's data is stored
func CharDataFile(char string) string {
	return CharDir(char) + "\\data"
}

// PasswordFile returns the path of the named user's password file
func PasswordFile(usr string) string {
	return UserDir(usr) + "\\pwd"
}

// AdminFile returns the path of the named user's admin file
func AdminFile(usr string) string {
	return UserDir(usr) + "\\admin"
}

// UsernameFile returns the path of the named user's username file
func UsernameFile(usr string) string {
	return UserDir(usr) + "\\username"
}

// CharnameFile returns the path of the named user's character name file
func CharnameFile(usr string) string {
	return UserDir(usr) + "\\charname"
}

// UsersDir returns the path to the users directory
func UsersDir() string {
	return ExePath() + "\\data\\users\\"
}

// CharsDir returns the path to the characters directory
func CharsDir() string {
	return ExePath() + "\\data\\chars\\"
}

func buildDataPath() {
	path := ExePath()
	_, err := ioutil.ReadDir(path + "\\data\\")
	if err != nil {
		os.Mkdir(path+"\\data\\", 0770)
	}
}
