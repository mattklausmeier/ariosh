package dataio

import (
	"io/ioutil"
	"os"
	"strings"
)

func init() {
	_, err := ioutil.ReadDir("./data/")
	if err != nil {
		err = os.Mkdir("./data/", 0770)
	}
	_, err = ioutil.ReadDir("./data/chars/")
	if err != nil {
		err = os.Mkdir("./data/chars/", 0770)
	}
	_, err = ioutil.ReadDir("./data/users/")
	if err != nil {
		err = os.Mkdir("./data/users/", 0770)
	}
}

// PathSeparator returns the path separator character for Linux
func PathSeparator() string {
	return "/"
}

// UserDir returns the specified user's directory
func UserDir(usr string) string {
	sha := Hash(strings.ToLower(usr))
	return UsersDir() + sha
}

// CharDir returns the specified char's directory
func CharDir(char string) string {
	sha := Hash(strings.ToLower(char))
	return CharsDir() + sha
}

// CharDataFile returns the file where the character's data is stored
func CharDataFile(char string) string {
	return CharDir(char) + "/data"
}

// PasswordFile returns the path of the specified user's password file
func PasswordFile(usr string) string {
	return UserDir(usr) + "/pwd"
}

// AdminFile returns the path of the specified user's admin file
func AdminFile(usr string) string {
	return UserDir(usr) + "/admin"
}

// UsernameFile returns the path of the specified user's username file
func UsernameFile(usr string) string {
	return UserDir(usr) + "/username"
}

// CharnameFile returns the path of the specified user's character name file
func CharnameFile(usr string) string {
	return UserDir(usr) + "/charname"
}

// UsersDir returns the path of the users directory
func UsersDir() string {
	return "./data/users/"
}

// CharsDir returns the path of the characters directory
func CharsDir() string {
	return "./data/chars/"
}
