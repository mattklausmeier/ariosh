package dataio

import (
	"crypto/sha512"
	"encoding/base64"
	"io/ioutil"
	"os"
)

// Hash returns a SHA-512 hash of the supplied string
func Hash(str string) string {
	hasher := sha512.New()
	hasher.Write([]byte(str))
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}

// UserDirExists returns true if the named user already exists in the system
func UserDirExists(usr string) bool {
	_, err := ioutil.ReadDir(UserDir(usr))
	if err != nil {
		return false
	}
	return true
}

// CreateUserDirectory creates a new user directory using the supplied username
func CreateUserDirectory(usr string) {
	if !UserDirExists(usr) {
		os.Mkdir(UserDir(usr), 0770)
		ioutil.WriteFile(UsernameFile(usr), []byte(usr), 0660)
	}
}

// DeleteUserDirectory deletes the user directory for the named user
func DeleteUserDirectory(usr string) {
	os.RemoveAll(UserDir(usr))
}

// CharDirExists returns true if the named character has a character directory
func CharDirExists(char string) bool {
	_, err := ioutil.ReadDir(CharDir(char))
	if err != nil {
		return false
	}
	return true
}

// CreateCharDirectory creates a character directory for the named user using
// the specified character name
func CreateCharDirectory(usr, char string) {
	if !CharDirExists(char) {
		os.Mkdir(CharDir(char), 0770)
		ioutil.WriteFile(CharnameFile(usr), []byte(char), 0660)
	}
}

// DeleteCharDirectory deletes the character directory of the named character
func DeleteCharDirectory(char string) {
	os.RemoveAll(CharDir(char))
}

// UserHasPassword returns true if the named user has set a password
// If this returns false, the user is still creating their account
func UserHasPassword(usr string) bool {
	file, err := os.Open(PasswordFile(usr))
	if err != nil {
		return false
	}
	file.Close()
	return true
}

// UserIsAdmin returns true if the user is a system administrator
func UserIsAdmin(usr string) bool {
	file, err := os.Open(AdminFile(usr))
	if err != nil {
		return false
	}
	file.Close()
	return true
}

// SavePassword sets the named user's password to the specified string
func SavePassword(usr, pass string) {
	ioutil.WriteFile(PasswordFile(usr), []byte(Hash(pass)), 0660)
}

// DeletePassword deletes the password for the named user
func DeletePassword(usr string) {
	os.Remove(PasswordFile(usr))
}

// PasswordCorrect returns true if the password string matches that of the named user
func PasswordCorrect(usr, pass string) bool {
	file, err := os.Open(PasswordFile(usr))
	if err != nil {
		return false
	}
	b := make([]byte, 88)
	file.Read(b)
	file.Close()
	if string(b) == Hash(pass) {
		return true
	}
	return false
}

// SaveChar saves a character's data to disk
func SaveChar(charname, race, class, gender, stats, inventory, lastRoom string) {
	ioutil.WriteFile(CharDataFile(charname), []byte(race+"\n"+class+"\n"+gender+"\n"+stats+"\n"+inventory+"\n"+lastRoom), 0660)
}

// ReadChar returns the contents of the specified character's data file
func ReadChar(charname string) string {
	data, err := ioutil.ReadFile(CharDataFile(charname))
	if err != nil {
		return ""
	}
	return string(data)
}
