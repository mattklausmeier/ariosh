In the Ariosh Online engine, changing text color is simple.
When you're creating a string, like so:

    "Hello, player!  Have a good day!"

You can surround any part of that string with square brackets that specify
in hex what color the contained text is going to be.  The following example
makes the word "player" red:

    "Hello, [ff0000]player[]!  Have a good day!"

The opening set of square brackets contains a 6 digit hex code describing what
color the text will be.  The empty, closing square brackets tells the engine
to revert to the default color (gray) at this point.

Color brackets can be nested.  Consider the following:

    "[ff0000]This is red, [00ff00]this is green,[] and this is red.[]"

The nested green color block overrides the outer red color block.  It does
not combine the colors, it overwrites it completely.  This allows a function to
return a preformatted color block which you can pste directly into your string,
as the following example demonstrates:

    "[ff0000]This is red, " + getUnknownColorString() + " and this is red.[]"

!!! Note that you must not use a hashtag as you would in HTML (#ffffff), and you
must not use any form of shorthand with fewer than 6 characters (in HTML, #fff is
legal, but it doesn't work here.)
