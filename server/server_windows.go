package server

import (
	"net/http"

	"golang.org/x/net/websocket"
)

// Run starts the web and websocket servers, passing the websocket
// connections to the supplied echoServer handler.
func Run(echoServer websocket.Handler) error {
	http.Handle("/", http.FileServer(http.Dir(".\\testhtml")))
	http.Handle("/echo", websocket.Handler(echoServer))
	return http.ListenAndServe("localhost:80", nil)
}
