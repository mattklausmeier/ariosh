package server

import (
	"net/http"

	"golang.org/x/net/websocket"
)

var domain = "arioshonline.com:1443"
var pubkey = "./ssl/cert.pem"
var privkey = "./ssl/privkey.pem"

// Run is the main server loop that handles incoming websocket connections
func Run(echoServer websocket.Handler) error {
	http.Handle("/echo", websocket.Handler(echoServer))
	err := make(chan error)
	go listenAndServeTLS(domain, pubkey, privkey, err)
	return <-err
}

func listenAndServeTLS(address, pubkey, privkey string, err chan error) {
	err <- http.ListenAndServeTLS(address, pubkey, privkey, nil)
}
