var msgBox = $("#messages");
var inputBox = $("#m");
var ws = new WebSocket('wss://arioshonline.com:1443/echo');
var element = $("#intro");
var echoCheck = $("#echoButton");
var header = $("#header");
var form = $("#form");
var connectionIcon = $("#connectionIcon");
var connectionStatus = $("#connectionStatus");
var infopanel = $("#infopanel");
var commandHistory = [];
var historyPointer = 0;
var headerDisplayed = false;
var tabcontent = $("#tabcontent");
var tabinv = $("#tab-inv");
var tabmap = $("#tab-map");
var tabstat = $("#tab-stat");
var tabcmds = $("#tab-cmds");
var inventory = $("#inventory");
var map = $("#map");
var stats = $("#stats");
var commands = $("#commands");
var colors = {
    "red":     "#ec4949", "salmon": "#c56868",
    "green":   "#668811", "blue":   "#2387d4",
    "cyan":    "#00b4cd", "command":"#00b4cd",
    "darkblue":"#00007f", "violet": "#c757a8",
    "orange":  "#d07800", "brown":  "#916300"
};
element.remove();
window.addEventListener("resize", redraw);

echoCheck.click(function(e) {
    toggleEcho();
    inputBox.focus();
});

function redraw() {
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    form.width(width);
    msgBox.width(width * 0.55);
    msgBox.height(height - form.height() - 1);
    header.width(width - msgBox.width());
    header.css("right", "0px");
    infopanel.width(width - msgBox.width());
    infopanel.height(height - header.outerHeight() - form.outerHeight());
    infopanel.css("top", header.outerHeight());
    infopanel.css("right", 0);

    var tabwidth = infopanel.width() * 0.25 - 9;
    var tabpad = 7;
    tabinv.width(tabwidth);
    tabinv.css("left", tabpad);
    tabmap.width(tabwidth);
    tabmap.css("left", tabwidth + tabpad * 2);
    tabstat.width(tabwidth);
    tabstat.css("left", tabwidth * 2 + tabpad * 3);
    tabcmds.width(tabwidth);
    tabcmds.css("left", tabwidth * 3 + tabpad * 4);

    tabcontent.height(infopanel.innerHeight() - tabinv.outerHeight() - 4);
    tabcontent.width(infopanel.outerWidth() - 4);
    tabcontent.css("top", tabinv.outerHeight());
    inventory.height(tabcontent.height());
    commands.height(tabcontent.height());
    map.width(infopanel.innerWidth() - 4);
    map.height(tabcontent.height());
}

function addColor(elem, input) {
    var depth = 0;
    var elems = [];
    elems.push(elem);
    for (var a = 0; a < input.length; a++) {
        if (input[a] == "[" && input.indexOf("]", a) > 0) {
            if (input.indexOf("]", a) == (a + 1) && depth > 0) {
                elems.pop();
                depth--;
                a++;
            } else if (isValidColor(input.substring(a + 1, input.indexOf("]", a)))) {
                elems.push(document.createElement("span"));
                elems[depth].appendChild(elems[depth + 1]);
                depth++;
                elems[depth].style["color"] = "#" + input.substring(a + 1, a + 7);
                a += 7;
            } else if (!headerDisplayed && input.substring(a + 1, input.indexOf("]", a)) == "header") {
                elem.style["text-align"] = "center";
                elems.push(document.createElement("span"));
                elems[depth].appendChild(elems[depth + 1]);
                depth++;
                elems[depth].style["font-family"] = "'Tangerine', cursive";
                elems[depth].style["font-size"] = "92px";
                a += 7;
                headerDisplayed = true;
            } else if (input.substring(a + 1, input.indexOf("]", a)) == "center") {
                elem.style["text-align"] = "center";
                a += 7;
            } else if (input.substring(a + 1, input.indexOf("]", a)) == "title") {
                elem.style["padding-left"] = "10px";
                elems.push(document.createElement("span"));
                elems[depth].appendChild(elems[depth + 1]);
                depth++;
                a += 6;
            } else if (input.substring(a + 1, input.indexOf("]", a)) == "hr") {
                elem.style["text-align"] = "center";
                elems.push(document.createElement("span"));
                elems[depth].appendChild(elems[depth + 1]);
                elems[depth + 1].insertAdjacentHTML('beforeend', "_______________________________________________");
                elems[depth + 1].style["position"] = "relative;top: -10px";
                elems[depth + 1].style["top"] = "-10px";
                elems.pop();
                a += 3;
            } else if (input.substring(a + 1, input.indexOf("]", a)) == "b") {
                elems.push(document.createElement("span"));
                elems[depth].appendChild(elems[depth + 1]);
                depth++;
                elems[depth].style["font-weight"] = "bold";
                a += 2;
            } else if (isPredefinedColor(input.substring(a + 1, input.indexOf("]", a)))) {
                elems.push(document.createElement("span"));
                elems[depth].appendChild(elems[depth + 1]);
                depth++;
                var key = input.substring(a + 1, input.indexOf("]", a));
                if (key in colors) {
                    elems[depth].style["color"] = colors[key];
                }
                a += input.substring(a + 1, input.indexOf("]", a)).length + 1;
            } else {
                elems[depth].insertAdjacentHTML('beforeend', input[a]);
            }
        } else {
            elems[depth].insertAdjacentHTML('beforeend', input[a]);
        }
    }
}

function isPredefinedColor(str) {
    return str == "red" || str == "salmon" || str == "green" || str == "blue" ||
        str == "cyan" || str == "darkblue" || str == "violet" || str == "orange" ||
        str == "brown" || str == "command";
}

function tabChange(tabId) {
    if ($.inArray(tabId, ["inv", "map", "stat", "cmds"]) == -1) {
        return;
    }
    inventory.addClass("hidden");
    map.addClass("hidden");
    stats.addClass("hidden");
    commands.addClass("hidden");
    tabinv.removeClass("selected");
    tabmap.removeClass("selected");
    tabstat.removeClass("selected");
    tabcmds.removeClass("selected");
    switch (tabId) {
        case "inv":
            inventory.removeClass("hidden");
            tabinv.addClass("selected");
            break;
        case "map":
            map.removeClass("hidden");
            tabmap.addClass("selected");
            break;
        case "stat":
            stats.removeClass("hidden");
            tabstat.addClass("selected");
            break;
        case "cmds":
            commands.removeClass("hidden");
            tabcmds.addClass("selected");
            break;
    }
}

function isValidColor(input) {
    if (input.length != 6) {
        return false;
    }
    var chars = "0123456789abcdefABCDEF";
    for (var a = 0; a < 6; a++) {
        if (chars.indexOf(input[a]) == -1) {
            return false;
        }
    }
    return true;
}

function toggleEcho() {
    if (echoCheck.html() == "Echo On") {
        echoCheck.html("Echo Off");
    } else {
        echoCheck.html("Echo On");
    }
}

function sendInput() {
    if (echoCheck.html() == "Echo On") {
        var li = document.createElement("li");
        li.className += " echo";
        var text = document.createTextNode(inputBox.val());
        li.appendChild(text);
        msgBox.append(li);
    }
    if ($("#mp").is(':visible')) {
        ws.send($("#mp").val());
        $("#mp").val("");
        $("#m").show();
        $("#m").focus()
        $("#mp").hide();
    } else {
        commandHistory.push(inputBox.val());
        if (commandHistory.length > 100) {
            commandHistory.shift();
        }
        ws.send(inputBox.val());
        inputBox.val("");
        historyPointer = commandHistory.length;
        inputBox.focus();
    }
}

ws.onopen = function() {
    connectionIcon.addClass("connected");
    connectionIcon.removeClass("notconnected");
    connectionStatus.html("Connected");
}

ws.onclose = function() {
    connectionIcon.addClass("notconnected");
    connectionIcon.removeClass("connected");
    connectionStatus.html("Not Connected");
    var elem = document.createElement("li");
    addColor(elem, "Connection Lost!");
    msgBox.append(elem);
    msgBox.scrollTop(msgBox.prop('scrollHeight'));
}

ws.onmessage = function(e) {
    if (elem != null) {
        $(elem).remove();
        elem = null;
    }
    if (e.data.length >= 5 && e.data.substring(0, 5) == "[inv]") {
        inventory.empty();
        var items = e.data.substring(5).split("|");
        for (var a = 0; a < items.length; a++) {
            var li = document.createElement("li");
            addColor(li, items[a]);
            inventory.append(li);
        }
    } else if (e.data.length >= 6 && e.data.substring(0, 6) == "[pass]") {
        $("#mp").show();
        $("#mp").focus()
        $("#m").hide();
        var li = document.createElement("li");
        addColor(li, e.data.substring(6));
        msgBox.append(li);
    } else {
        var li = document.createElement("li");
        addColor(li, e.data);
        msgBox.append(li);
        msgBox.scrollTop(msgBox.prop('scrollHeight'));
    }
}

document.onkeydown = function(e) {
    e = e || window.event;
    if (e.keyCode == '38') {
        // Up Arrow
        if (historyPointer > 0) {
            historyPointer--;
            inputBox.val(commandHistory[historyPointer]);
        }
    } else if (e.keyCode == '40') {
        // Down Arrow
        if (historyPointer < commandHistory.length) {
            historyPointer++;
            if (historyPointer == commandHistory.length) {
                inputBox.val("");
            } else {
                inputBox.val(commandHistory[historyPointer]);
            }
        }
    } else if (e.keyCode == '13') {
        sendInput();
    }
}

inputBox.focus();
var elem = document.createElement("li");
addColor(elem, "Attempting to connect...");
msgBox.append(elem);
redraw();
