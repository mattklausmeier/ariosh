package intf

import "golang.org/x/net/websocket"

// Player represents a user's online presense.
type Player interface {
	SendToPlayer(...string)
	DidSendFail() bool
	HandlePlayerInput(string)
	SetRoom(Room)
	GetRoom() Room
	SetGame(PlayerHandler)
	GetGame() PlayerHandler
	SetChar(Char)
	GetChar() *Char
	SetUsername(string)
	GetUsername() string
	SetLoggedIn(bool)
	GetLoggedIn() bool
	SetConn(*websocket.Conn)
	GetConn() *websocket.Conn
	Heartbeat()
}
