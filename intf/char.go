package intf

import (
	"bytes"
	"strconv"
	"strings"
)

// Char represents a User's character in the game
type Char struct {
	Name      string
	Inventory []Item
	Class     ClassType
	Race      RaceType
	Gender    GenderType
	Str       int
	Dex       int
	Con       int
	Cha       int
	Mag       int
	Luc       int
}

// RaceType enumerates the different types of races that a player can be
type RaceType uint

// The different races
const (
	NoRace    RaceType = 0
	Dwarf              = 1
	HalfDwarf          = 2
	Elf                = 3
	HalfElf            = 4
	Orc                = 5
	HalfOrc            = 6
	Human              = 7
	Drakkin            = 8
	Fokhai             = 9
)

// ClassType enumerates the different types of classes that a player can be
type ClassType uint

// The different classes
const (
	NoClass ClassType = 0
	Fighter           = 1
	Wizard            = 2
	Cleric            = 3
	Thief             = 4
)

// GenderType enumerates the possible gender types
type GenderType bool

// The different genders
const (
	Male   GenderType = false
	Female            = true
)

// TakeItem add the specified item to the character's inventory
func (c *Char) TakeItem(i Item) {
	if i == nil {
		return
	}
	c.Inventory = append(c.Inventory, i)
}

// LoseItem removes the specified item from the character's inventory
func (c *Char) LoseItem(i Item) {
	if i == nil {
		return
	}
	length := len(c.Inventory)
	for a := 0; a < length; a++ {
		if c.Inventory[a] == i {
			if a < length-1 {
				c.Inventory[a] = c.Inventory[length-1]
			}
			c.Inventory = c.Inventory[:length-1]
			return
		}
	}
}

// MarshallInventory converts the character's inventory into a string for
// saving to disk.
func (c *Char) MarshallInventory() string {
	var buffer bytes.Buffer
	for _, item := range c.Inventory {
		buffer.WriteString(item.Marshall(item.ItemID()))
	}
	return buffer.String()
}

// GetRace returns the character's race as a RaceType enum
func (c *Char) GetRace() RaceType {
	return c.Race
}

// GetRaceString returns the character's race as a string
func (c *Char) GetRaceString() string {
	switch c.Race {
	case Dwarf:
		return "Dwarf"
	case HalfDwarf:
		return "Half-Dwarf"
	case Elf:
		return "Elf"
	case HalfElf:
		return "Half-Elf"
	case Orc:
		return "Orc"
	case HalfOrc:
		return "Half-Orc"
	case Human:
		return "Human"
	case Drakkin:
		return "Drakkin"
	case Fokhai:
		return "Fokhai"
	}
	return ""
}

// SetRace sets the character's race based on the supplied RaceType enum
func (c *Char) SetRace(rt RaceType) {
	c.Race = rt
}

// SetRaceString sets the character's race based on the supplied string
func (c *Char) SetRaceString(rt string) {
	switch rt {
	case "Dwarf":
		c.Race = Dwarf
	case "Half-Dwarf":
		c.Race = HalfDwarf
	case "Elf":
		c.Race = Elf
	case "Half-Elf":
		c.Race = HalfElf
	case "Orc":
		c.Race = Orc
	case "Half-Orc":
		c.Race = HalfOrc
	case "Human":
		c.Race = Human
	case "Drakkin":
		c.Race = Drakkin
	case "Fokhai":
		c.Race = Fokhai
	}
	c.Race = NoRace
}

// GetClass returns the character's class as a ClassType enum
func (c *Char) GetClass() ClassType {
	return NoClass
}

// GetClassString returns the character's class as a string
func (c *Char) GetClassString() string {
	switch c.Class {
	case Fighter:
		return "Fighter"
	case Wizard:
		return "Wizard"
	case Cleric:
		return "Cleric"
	case Thief:
		return "Thief"
	}
	return ""
}

// SetClass sets the character's class based on the supplied ClassType enum
func (c *Char) SetClass(ct ClassType) {
	c.Class = ct
}

// SetClassString sets the character's class based on the supplied string
func (c *Char) SetClassString(ct string) {
	switch ct {
	case "Fighter":
		c.Class = Fighter
	case "Wizard":
		c.Class = Wizard
	case "Cleric":
		c.Class = Cleric
	case "Thief":
		c.Class = Thief
	}
	c.Class = NoClass
}

// SetGender sets the character's gender based on an enum
func (c *Char) SetGender(g GenderType) {
	c.Gender = g
}

// SetGenderString sets the character's gender based on a string
func (c *Char) SetGenderString(s string) {
	if s == "Female" {
		c.Gender = Female
	} else {
		c.Gender = Male
	}
}

// GetGender returns the character's gender as a GenderType
func (c *Char) GetGender() GenderType {
	return c.Gender
}

// GetGenderString returns the character's gender as a string
func (c *Char) GetGenderString() string {
	switch c.Gender {
	case Male:
		return "Male"
	default:
		return "Female"
	}
}

// SetStats takes a space-separated string of 6 integers and sets those as
// the character's stats
func (c *Char) SetStats(s string) {
	stats := strings.Split(s, " ")
	if len(stats) != 6 {
		c.SetStr(10)
		c.SetDex(10)
		c.SetCon(10)
		c.SetCha(10)
		c.SetMag(10)
		c.SetLuc(10)
		return
	}
	i, err := strconv.Atoi(stats[0])
	if err == nil {
		c.SetStr(i)
	}
	i, err = strconv.Atoi(stats[1])
	if err == nil {
		c.SetDex(i)
	}
	i, err = strconv.Atoi(stats[2])
	if err == nil {
		c.SetCon(i)
	}
	i, err = strconv.Atoi(stats[3])
	if err == nil {
		c.SetCha(i)
	}
	i, err = strconv.Atoi(stats[4])
	if err == nil {
		c.SetMag(i)
	}
	i, err = strconv.Atoi(stats[5])
	if err == nil {
		c.SetLuc(i)
	}
}

// GetStatString returns all of the player's stats as one string
func (c *Char) GetStatString() string {
	return strconv.Itoa(c.GetStr()) + " " + strconv.Itoa(c.GetDex()) + " " + strconv.Itoa(c.GetCon()) + " " + strconv.Itoa(c.GetCha()) + " " + strconv.Itoa(c.GetMag()) + " " + strconv.Itoa(c.GetLuc())
}

// SetStr sets the character's strength
func (c *Char) SetStr(i int) {
	c.Str = i
}

// GetStr gets the character's strength
func (c *Char) GetStr() int {
	return c.Str
}

// SetDex sets the character's strength
func (c *Char) SetDex(i int) {
	c.Dex = i
}

// GetDex gets the character's strength
func (c *Char) GetDex() int {
	return c.Dex
}

// SetCon sets the character's strength
func (c *Char) SetCon(i int) {
	c.Con = i
}

// GetCon gets the character's strength
func (c *Char) GetCon() int {
	return c.Con
}

// SetCha sets the character's strength
func (c *Char) SetCha(i int) {
	c.Cha = i
}

// GetCha gets the character's strength
func (c *Char) GetCha() int {
	return c.Cha
}

// SetMag sets the character's strength
func (c *Char) SetMag(i int) {
	c.Mag = i
}

// GetMag gets the character's strength
func (c *Char) GetMag() int {
	return c.Mag
}

// SetLuc sets the character's strength
func (c *Char) SetLuc(i int) {
	c.Luc = i
}

// GetLuc gets the character's strength
func (c *Char) GetLuc() int {
	return c.Luc
}

// FindItem finds an item in the player's inventory by name
func (c *Char) FindItem(s string) Item {
	for a := 0; a < len(c.Inventory); a++ {
		if c.Inventory[a] != nil && c.Inventory[a].GetDescription(SimpleName) == s {
			return c.Inventory[a]
		}
	}
	return nil
}

// Inv tells the player everything in their inventory
func (c *Char) Inv(p Player) {
	if p == nil {
		return
	}
	retstr := "[inv]"
	items := map[string]int{}
	for a := 0; a < len(c.Inventory); a++ {
		if c.Inventory[a] != nil {
			items[c.Inventory[a].GetDescription(SimpleName)]++
		}
	}
	for name, amnt := range items {
		retstr += strconv.Itoa(amnt) + " x " + name + "|"
	}
	if len(c.Inventory) > 0 {
		p.SendToPlayer(retstr[:len(retstr)-1])
	} else {
		p.SendToPlayer(retstr[:len(retstr)])
	}
}
