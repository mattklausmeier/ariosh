package intf

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
)

// Direction is a constant representing a direction of travel.
type Direction int

// Directional constants
const (
	DirNorth Direction = 0
	DirSouth           = 1
	DirEast            = 2
	DirWest            = 3
	DirUp              = 4
	DirDown            = 5
)

// Room describes an object that behaves as a game room.
type Room interface {
	TakePlayer(p Player)
	LosePlayer(p Player)
	HandleInput(p Player, str string) bool
	PlayersInRoom() []Player
	GetPlayerExit(p Player, d Direction) roomid.RoomType
	TakeItem(i Item)
	LoseItem(i Item)
	GetInventory() []Item
	Look(p Player)
	ShouldDelete() bool
	RoomIdString() string
}
