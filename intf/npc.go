package intf

// NPC represents any server creature in the game.
type NPC interface {
	Name() string
}
