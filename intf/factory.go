package intf

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/itemid"
	"bitbucket.org/mattklausmeier/ariosh/ids/npcid"
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
)

// Factory creates specified objects of generic types
type Factory interface {
	Room(roomid.RoomType) Room
	Player() Player
	Item(itemid.ItemType) Item
	NPC(npcid.NPCType) NPC
}
