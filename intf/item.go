package intf

import "bitbucket.org/mattklausmeier/ariosh/ids/itemid"

// Item represents anything in the world that can be picked up, used,
// or stored in an inventory.  (Basically anything other than characters.)
type Item interface {
	GetDescription(DescriptionType) string
	Marshall(itemid.ItemType) string
	Unmarshall(string)
	ItemID() itemid.ItemType
}

// DescriptionType enumerates the different types of descriptions that
// can be returned when referencing the object
type DescriptionType int

// SimpleName is the shortest way of describing the item, e.g. "coal"
// BriefDescription is slightly more poetic, e.g. "lump of coal"
// LookDescription is if the user "look <item>", e.g. "a rough, half-pound lump of coal"
const (
	SimpleName       DescriptionType = 0
	BriefDescription                 = 1
	LookDescription                  = 2
)

// RemoveItemFromSlice takes a slice of Items, removes the specified one (if found),
// and returns the rest
func RemoveItemFromSlice(i Item, s []Item) []Item {
	if i == nil || s == nil {
		return s
	}
	length := len(s)
	for a := 0; a < length; a++ {
		if s[a] != nil && s[a] == i {
			if a < length-1 {
				s[a] = s[length-1]
			}
			return s[:length-1]
		}
	}
	// Player not found, return unmodified array
	return s
}

// RemoveNPCFromSlice takes a slice of NPCs, removes the specified one (if found),
// and returns the rest
func RemoveNPCFromSlice(i NPC, s []NPC) []NPC {
	if i == nil || s == nil {
		return s
	}
	length := len(s)
	for a := 0; a < length; a++ {
		if s[a] != nil && s[a] == i {
			if a < length-1 {
				s[a] = s[length-1]
			}
			return s[:length-1]
		}
	}
	// NPC not found, return unmodified array
	return s
}
