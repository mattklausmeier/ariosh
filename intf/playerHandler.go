package intf

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"golang.org/x/net/websocket"
)

// PlayerHandler handles players
type PlayerHandler interface {
	AddPlayer(ws *websocket.Conn) Player
	RemovePlayer(p Player)
	LoggedInPlayerCount() int
	PlayerIsOnline(str string) bool
	MovePlayerToRoom(p Player, room roomid.RoomType)
	SaveChar(Player)
}

// RemovePlayerFromSlice is a helper function that returns a []Player
// after the specified Player has been removed.
func RemovePlayerFromSlice(p Player, s []Player) []Player {
	if p == nil || s == nil {
		return s
	}
	length := len(s)
	for a := 0; a < length; a++ {
		if s[a] != nil && s[a] == p {
			if a < length-1 {
				s[a] = s[length-1]
			}
			return s[:length-1]
		}
	}
	// Player not found, return unmodified array
	return s
}
