package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// East moves this player through his room's east exit.
func East(p intf.Player) {
	if p == nil || p.GetRoom() == nil || p.GetGame() == nil {
		return
	}
	room := p.GetRoom().GetPlayerExit(p, intf.DirEast)
	if room == roomid.NoExit {
		p.SendToPlayer("You can't go east!")
	} else {
		p.GetGame().MovePlayerToRoom(p, room)
	}
}
