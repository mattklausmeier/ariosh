package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// Look tells the player the
func Look(p intf.Player) {
	if p == nil || p.GetRoom() == nil {
		return
	}
	p.GetRoom().Look(p)
}
