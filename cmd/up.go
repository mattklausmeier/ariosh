package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// Up moves this player through his room's up exit.
func Up(p intf.Player) {
	if p == nil || p.GetRoom() == nil || p.GetGame() == nil {
		return
	}
	room := p.GetRoom().GetPlayerExit(p, intf.DirUp)
	if room == roomid.NoExit {
		p.SendToPlayer("You can't go up!")
	} else {
		p.GetGame().MovePlayerToRoom(p, room)
	}
}
