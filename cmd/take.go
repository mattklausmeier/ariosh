package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// Take searches the player's room for the specified item, and
// transfers it to his inventory if it is found
func Take(p intf.Player, item string) {
	if p == nil || p.GetRoom() == nil || p.GetChar() == nil {
		return
	}
	roominv := p.GetRoom().GetInventory()
	var found = false
	length := len(roominv)
	var itemref intf.Item
	for a := 0; a < length && !found; a++ {
		if roominv[a].GetDescription(intf.SimpleName) == item {
			itemref = roominv[a]
			p.GetRoom().LoseItem(itemref)
			found = true
		}
	}
	if !found {
		p.SendToPlayer("Couldn't find \"" + item + "\"")
		return
	}
	p.GetChar().TakeItem(itemref)
	p.GetChar().Inv(p)
	p.SendToPlayer("You picked up the " + itemref.GetDescription(intf.SimpleName))
}
