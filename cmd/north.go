package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// North moves this player through his room's north exit.
func North(p intf.Player) {
	if p == nil || p.GetRoom() == nil || p.GetGame() == nil {
		return
	}
	room := p.GetRoom().GetPlayerExit(p, intf.DirNorth)
	if room == roomid.NoExit {
		p.SendToPlayer("You can't go north!")
	} else {
		p.GetGame().MovePlayerToRoom(p, room)
	}
}
