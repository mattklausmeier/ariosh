package cmd

import "bitbucket.org/mattklausmeier/ariosh/intf"

// Help displays information on how to play the game.
func Help(p intf.Player) {
	if p == nil {
		return
	}
	var help = "[ffffff]north[] (or [ffffff]n[]) [ffffff]- []move through your room's north exit\n"
	help += "[ffffff]south[] (or [ffffff]s[]) [ffffff]- []move through your room's south exit\n"
	help += "[ffffff]east[] (or [ffffff]e[]) [ffffff]- []move through your room's east exit\n"
	help += "[ffffff]west[] (or [ffffff]w[]) [ffffff]- []move through your room's west exit\n"
	help += "[ffffff]up[] (or [ffffff]u[]) [ffffff]- []move through your room's up exit\n"
	help += "[ffffff]down[] (or [ffffff]d[]) [ffffff]- []move through your room's down exit\n"
	help += "[ffffff]who - []lists all the people in your current room\n"
	p.SendToPlayer(help)
}
