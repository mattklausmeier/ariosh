package cmd

import "bitbucket.org/mattklausmeier/ariosh/intf"

// Who returns the names of all other players in this player's room.
func Who(p intf.Player) {
	if p == nil || p.GetRoom() == nil {
		return
	}
	who := ""
	players := p.GetRoom().PlayersInRoom()
	for a := 0; a < len(players); a++ {
		if players[a].GetChar() == nil {
			continue
		}
		who += players[a].GetChar().Name
		if a < len(players)-1 {
			who += ", "
		}
	}
	p.SendToPlayer(who)
}
