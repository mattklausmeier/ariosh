package cmd

import (
	"io/ioutil"
	"log"
	"os"

	"bitbucket.org/mattklausmeier/ariosh/dataio"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// ListUsers returns the list of every created username.
// This function only works for an admin user.
func ListUsers(p intf.Player) {
	if p == nil {
		return
	}
	if dataio.UserIsAdmin(p.GetUsername()) {
		dirs, err := ioutil.ReadDir(dataio.UsersDir())
		if err != nil {
			log.Fatal(err)
		}
		who := ""
		for _, dir := range dirs {
			file, err2 := os.Open(dataio.UsersDir() + dir.Name() + dataio.PathSeparator() + "username")
			if err2 == nil {
				b := make([]byte, 20)
				file.Read(b)
				file.Close()
				who += string(b)
				file.Close()
			}
			file, err2 = os.Open(dataio.UsersDir() + dir.Name() + dataio.PathSeparator() + "charname")
			if err2 == nil {
				b := make([]byte, 20)
				file.Read(b)
				file.Close()
				who += ", " + string(b)
				file.Close()
			}
			who += "\n"
		}
		p.SendToPlayer(who)
	} else {
		p.SendToPlayer("What?")
	}
}
