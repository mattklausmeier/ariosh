package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// Drop searches the player's inventory for the specified item, and
// transfers it to the room's inventory if it is found
func Drop(p intf.Player, item string) {
	if p == nil || p.GetRoom() == nil || p.GetChar() == nil {
		return
	}
	itemref := p.GetChar().FindItem(item)
	if itemref == nil {
		p.SendToPlayer("You don't seem to have \"" + item + "\"")
		return
	}
	p.GetChar().LoseItem(itemref)
	p.GetChar().Inv(p)
	p.GetRoom().TakeItem(itemref)
	p.SendToPlayer("You dropped the " + itemref.GetDescription(intf.SimpleName))
}
