package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// West moves this player through his room's west exit.
func West(p intf.Player) {
	if p == nil || p.GetRoom() == nil || p.GetGame() == nil {
		return
	}
	room := p.GetRoom().GetPlayerExit(p, intf.DirWest)
	if room == roomid.NoExit {
		p.SendToPlayer("You can't go west!")
	} else {
		p.GetGame().MovePlayerToRoom(p, room)
	}
}
