package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// South moves this player through his room's south exit.
func South(p intf.Player) {
	if p == nil || p.GetRoom() == nil || p.GetGame() == nil {
		return
	}
	room := p.GetRoom().GetPlayerExit(p, intf.DirSouth)
	if room == roomid.NoExit {
		p.SendToPlayer("You can't go south!")
	} else {
		p.GetGame().MovePlayerToRoom(p, room)
	}
}
