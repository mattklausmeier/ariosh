package cmd

import (
	"strings"

	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// Say sends this player's message to everyone in his room.
func Say(p intf.Player, msg string) {
	if p == nil || p.GetRoom() == nil || p.GetChar() == nil {
		return
	}
	cmds := strings.SplitN(strings.TrimSpace(msg), " ", 2)
	if len(cmds) < 2 {
		p.SendToPlayer("Say what?")
		return
	}
	players := p.GetRoom().PlayersInRoom()
	for a := 0; a < len(players); a++ {
		if players[a] == nil {
			continue
		}
		players[a].SendToPlayer("[title][009b72]" + p.GetChar().Name + ":")
		players[a].SendToPlayer(cmds[1])
	}
}
