package cmd

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// Down moves this player through his room's down exit.
func Down(p intf.Player) {
	if p == nil || p.GetRoom() == nil || p.GetGame() == nil {
		return
	}
	room := p.GetRoom().GetPlayerExit(p, intf.DirDown)
	if room == roomid.NoExit {
		p.SendToPlayer("You can't go down!")
	} else {
		p.GetGame().MovePlayerToRoom(p, room)
	}
}
