package rooms

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomNewCharGender is where the user selects their character's gender
type RoomNewCharGender struct {
	BaseRoom
}

// TakePlayer adds a player to this room
func (r *RoomNewCharGender) TakePlayer(p intf.Player) {
	if p == nil {
		return
	}
	r.BaseRoom.TakePlayer(p, r)
	r.prompt(p)
}

func (r *RoomNewCharGender) prompt(p intf.Player) {
	if p == nil {
		return
	}
	p.SendToPlayer(" ", "[title][blue]Choose Your Player's Gender")
	p.SendToPlayer("At least once a year, your mother recounts the story (much to your father's chagrin) of the friendly wager they made before knowing if they were having a boy or a girl.  Your mother got a month's worth of foot massages by correctly guessing that you would be:")
	p.SendToPlayer("  [darkblue]1) A boy.")
	p.SendToPlayer("  [darkblue]2) A girl.")
}

// HandleInput handles the player's input
func (r *RoomNewCharGender) HandleInput(p intf.Player, s string) bool {
	if p == nil || p.GetChar() == nil || p.GetGame() == nil {
		return false
	}
	switch s {
	case "1":
		p.GetChar().SetGender(intf.Male)
	case "2":
		p.GetChar().SetGender(intf.Female)
	default:
		p.SendToPlayer(" ", "[title][red][b]Please select a number 1-2")
		r.prompt(p)
		return true
	}
	p.SendToPlayer(" ", "[title][blue]You are a [salmon]"+p.GetChar().GetGenderString())
	p.GetGame().MovePlayerToRoom(p, roomid.NewCharStats)
	return true
}
