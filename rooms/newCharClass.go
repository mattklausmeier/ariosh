package rooms

import (
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomNewCharClass is where the player chooses their class
type RoomNewCharClass struct {
	BaseRoom
}

// TakePlayer adds a player to this room
func (r *RoomNewCharClass) TakePlayer(p intf.Player) {
	if p == nil {
		return
	}
	r.BaseRoom.TakePlayer(p, r)
	r.prompt(p)
}

func (r *RoomNewCharClass) prompt(p intf.Player) {
	if p == nil {
		return
	}
	p.SendToPlayer(" ", "[title][blue]Choose Your Character's Class")
	p.SendToPlayer("When you first attempted to mimic your father in his profession, you were good for little more than a laugh.  But he could barely contain his pride on the day you sucessfully acommplished what?")
	p.SendToPlayer("  [darkblue]1) Riding out with the city watch to track down some maurading goblins. (Fighter)")
	p.SendToPlayer("  [darkblue]2) Conjuring the flame to ignite the winter festival bonfire. (Wizard)")
	p.SendToPlayer("  [darkblue]3) Healing your best friend's arm after he fell out of a tree. (Cleric)")
	p.SendToPlayer("  [darkblue]4) Picking the door to the cell where your father was being held prisoner. (Thief)")
}

// HandleInput handles user input while in this room
func (r *RoomNewCharClass) HandleInput(p intf.Player, str string) bool {
	if p == nil || p.GetChar() == nil || p.GetGame() == nil {
		return false
	}
	found := false
	switch str {
	case "1":
		p.GetChar().SetClass(intf.Fighter)
		found = true
	case "2":
		p.GetChar().SetClass(intf.Wizard)
		found = true
	case "3":
		p.GetChar().SetClass(intf.Cleric)
		found = true
	case "4":
		p.GetChar().SetClass(intf.Thief)
		found = true
	}
	if found {
		p.SendToPlayer(" ", "[title][blue]You are a [salmon]"+p.GetChar().GetClassString())
		p.GetGame().MovePlayerToRoom(p, roomid.NewCharGender)
	} else {
		p.SendToPlayer(" ", "[title][red][b]Please select a number 1-4")
		r.prompt(p)
	}
	return true
}
