package rooms

import (
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomWhitehallFountain is the fountain at the center of Whitehall
type RoomWhitehallFountain struct {
	BaseRoom
}

// TakePlayer adds a player to the room and announces the room location
func (r *RoomWhitehallFountain) TakePlayer(p intf.Player) {
	if p == nil {
		return
	}
	for a := 0; a < len(r.players); a++ {
		if r.players[a] != nil {
			r.players[a].SendToPlayer("[007f00]" + p.GetChar().Name + " joined the room.")
		}
	}
	r.BaseRoom.TakePlayer(p, r)
	p.SendToPlayer(" ", "[cyan][title]Whitehall Fountain")
	r.Look(p)
}

// Look describes the room and its contents to the player
func (r *RoomWhitehallFountain) Look(p intf.Player) {
	if p == nil {
		return
	}
	p.SendToPlayer("Before you, the cobblestone road is interrupted up by a circular, marble pool, prominently featuring a statue of a dragon wrestling an armored knight.  Water sprays from the mouth of a large dragon, and the knight wears no helmet, revealing a bearded, bald man.")
}
