package rooms

import (
	"strconv"
	"strings"

	"bitbucket.org/mattklausmeier/ariosh/dataio"
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomLoginUsername is the main room and handles the username portion of the login process
type RoomLoginUsername struct {
	BaseRoom
}

// TakePlayer adds a player to this room
func (r *RoomLoginUsername) TakePlayer(p intf.Player) {
	if p == nil || p.GetGame() == nil {
		return
	}
	r.players = append(r.players, p)
	p.SetRoom(r)
	p.SendToPlayer(" ", "[header]   Ariosh Online[] [e2e0d6]v0.0.2[]")
	p.SendToPlayer("[center][darkblue]World Builder: [brown]Aaron Ryker  [darkblue]Programmer: [brown]Matt Klausmeier")
	p.SendToPlayer("[hr]")
	p.SendToPlayer(" ", "[green]Ariosh Online[] is a [salmon]MUD[]: an online text adventure game,")
	p.SendToPlayer("an rpg that you play in your web browser with your friends.")
	p.SendToPlayer(" ", "[orange](We're still under construction, so there's not much to see yet.)")
	count := p.GetGame().LoggedInPlayerCount()
	p.SendToPlayer(" ", "There are [green][b]"+strconv.Itoa(count)+"[][] users logged in")
	p.SendToPlayer(" ", "To begin, please enter your username, or type [command][b]new[][] to create an account:")
}

// HandleInput handles user input while in this room
func (r *RoomLoginUsername) HandleInput(p intf.Player, str string) bool {
	if p == nil || p.GetGame() == nil {
		return false
	}
	if strings.ToLower(str) == "new" {
		p.GetGame().MovePlayerToRoom(p, roomid.NewUser)
	} else if p.GetGame().PlayerIsOnline(str) {
		p.SendToPlayer(" ", "[ff0000]Sorry, that user is already online.")
		p.SendToPlayer("Please enter your username, or type [command][b]new[][] to create an account:")
	} else if dataio.UserDirExists(str) {
		p.SetUsername(str)
		p.GetGame().MovePlayerToRoom(p, roomid.LoginPassword)
	} else {
		p.SendToPlayer(" ", "[ff0000]Sorry, we couldn't find that username!")
		p.SendToPlayer("Please enter your username, or type [command][b]new[][] to create an account:")
	}
	return true
}
