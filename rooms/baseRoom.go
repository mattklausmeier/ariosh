package rooms

import (
	"strconv"
	"time"

	"bitbucket.org/mattklausmeier/ariosh/game"

	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// BaseRoom is a base room struct that all other rooms should inherit from.
type BaseRoom struct {
	players        []intf.Player
	items          []intf.Item
	npcs           []intf.NPC
	Factory        intf.Factory
	poked          bool
	timeoutCounter int
	Game           game.Game
	shouldDelete   bool
	RoomId         roomid.RoomType
}

type heartbeat func()

// TakePlayer adds the specified player to this room's player array,
// and then sets the player's room reference to this room.
func (r *BaseRoom) TakePlayer(p intf.Player, rm intf.Room) {
	if p == nil {
		return
	}
	r.players = append(r.players, p)
	p.SetRoom(rm)
}

// LosePlayer removes the specified player from this room's player array.
func (r *BaseRoom) LosePlayer(p intf.Player) {
	if p == nil {
		return
	}
	r.players = intf.RemovePlayerFromSlice(p, r.players)
}

// HandleInput handles any special input for this room
// based on the specified player.
func (r *BaseRoom) HandleInput(p intf.Player, str string) bool {
	return false
}

// PlayersInRoom returns the array of players in this room.
func (r *BaseRoom) PlayersInRoom() []intf.Player {
	return r.players
}

// GetPlayerExit gets this room's exits that are available to this player.
func (r *BaseRoom) GetPlayerExit(p intf.Player, d intf.Direction) roomid.RoomType {
	return roomid.NoExit
}

// TakeItem is called when an item is added to the room's inventory
func (r *BaseRoom) TakeItem(i intf.Item) {
	if i == nil {
		return
	}
	r.items = append(r.items, i)
}

// LoseItem is called when an item is remeoved from the room's inventory
func (r *BaseRoom) LoseItem(i intf.Item) {
	if i == nil {
		return
	}
	r.items = intf.RemoveItemFromSlice(i, r.items)
}

// GetInventory returns the room's inventory
func (r *BaseRoom) GetInventory() []intf.Item {
	return r.items
}

// Look describes the room and its inventory to the player
func (r *BaseRoom) Look(p intf.Player) {

}

// Poke starts this room's heartbeat
func (r *BaseRoom) Poke(h heartbeat) {
	if !r.poked {
		r.poked = true
		ticker := time.NewTicker(1 * time.Second)
		go func() {
			for range ticker.C {
				if h != nil {
					h()
				}
				r.timeout()
			}
		}()
	}
}

func (r *BaseRoom) timeout() {
	if len(r.players) == 0 {
		r.timeoutCounter++
		if r.timeoutCounter >= 300 {
			r.shouldDelete = true
		}
	} else {
		r.timeoutCounter = 0
	}
}

// ShouldDelete returns true if this room has timed out and should be deleted
func (r *BaseRoom) ShouldDelete() bool {
	return r.shouldDelete && len(r.players) == 0
}

// TakeNPC is called when an NPC enters the room
func (r *BaseRoom) TakeNPC(i intf.NPC) {
	if i == nil {
		return
	}
	r.npcs = append(r.npcs, i)
}

// LoseNPC is called when an NPC leaves the room
func (r *BaseRoom) LoseNPC(i intf.NPC) {
	if i == nil {
		return
	}
	r.npcs = intf.RemoveNPCFromSlice(i, r.npcs)
}

// RoomIdString returns the room id as a string representation of the integer
// matching the room id's enum
func (r *BaseRoom) RoomIdString() string {
	return strconv.Itoa(int(r.RoomId))
}
