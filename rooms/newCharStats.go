package rooms

import (
	"strconv"
	"strings"

	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"

	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomNewCharStats is where a player chooses their character's stats
type RoomNewCharStats struct {
	BaseRoom
	phase map[intf.Player]int
}

// TakePlayer adds a player to this room
func (r *RoomNewCharStats) TakePlayer(p intf.Player) {
	if p == nil || p.GetChar() == nil {
		return
	}
	r.BaseRoom.TakePlayer(p, r)
	if r.phase == nil {
		r.phase = make(map[intf.Player]int)
	}
	r.phase[p] = 0
	p.GetChar().SetStr(10)
	p.GetChar().SetDex(10)
	p.GetChar().SetCon(10)
	p.GetChar().SetCha(10)
	p.GetChar().SetMag(10)
	p.GetChar().SetLuc(10)
	r.prompt(p)
}

// LosePlayer removes a player from the room
func (r *RoomNewCharStats) LosePlayer(p intf.Player) {
	if p == nil {
		return
	}
	r.BaseRoom.LosePlayer(p)
	delete(r.phase, p)
}

func (r *RoomNewCharStats) formatStats(s string) string {
	stats := strings.Split(s, " ")
	return "[title][b][orange]STR:[] [blue]" + stats[0] + " [orange]DEX:[] " + stats[1] + " [orange]CON:[] " + stats[2] + " [orange]CHA:[] " + stats[3] + " [orange]MAG:[] " + stats[4] + " [orange]LUC:[] " + stats[5]
}

// HandleInput handles the player's input
func (r *RoomNewCharStats) HandleInput(p intf.Player, s string) bool {
	if p == nil || p.GetChar() == nil {
		return false
	}
	switch r.phase[p] {
	case 0:
		r.phase[p] = 1
	default:
		if r.validNum(p, s) {
			r.incStat(p, s)
			r.phase[p]++
			p.SendToPlayer(" ", r.formatStats(p.GetChar().GetStatString()))
		} else {
			p.SendToPlayer(" ", "[title][red][b]Please choose a valid number")
		}
	}
	if r.phase[p] >= 7 && p.GetGame() != nil {
		p.GetGame().MovePlayerToRoom(p, roomid.NewCharName)
	} else {
		r.prompt(p)
	}
	return true
}

func (r *RoomNewCharStats) validNum(p intf.Player, s string) bool {
	if p == nil || p.GetChar() == nil {
		return false
	}
	i, err := strconv.Atoi(s)
	if err != nil {
		return false
	}
	count := 0
	if p.GetChar().GetStr() < 14 {
		count++
	}
	if p.GetChar().GetDex() < 14 {
		count++
	}
	if p.GetChar().GetCon() < 14 {
		count++
	}
	if p.GetChar().GetCha() < 14 {
		count++
	}
	if p.GetChar().GetMag() < 14 {
		count++
	}
	if p.GetChar().GetLuc() < 14 {
		count++
	}
	return i <= count && i > 0
}

func (r *RoomNewCharStats) prompt(p intf.Player) {
	if p == nil {
		return
	}
	p.SendToPlayer(" ", "[title][blue]Choose your Character's Stats")
	switch r.phase[p] {
	case 0:
		p.SendToPlayer("When you interact with the world, you do so using a custom variety of skills and strengths.  Those skills and strengths are described using the following 6 stats:")
		p.SendToPlayer("  [orange]Strength[] - [green]Your ability to apply physical force.")
		p.SendToPlayer("  [orange]Dexterity[] - [green]Your deftness, nimbleness, and fine-motor control.")
		p.SendToPlayer("  [orange]Constitution[] - [green]How tough and resiliant you are.")
		p.SendToPlayer("  [orange]Charisma[] - [green]Your charming way with people and animals.")
		p.SendToPlayer("  [orange]Magic[] - [green]Your ability to bend the mysterious forces of the universe to your will.")
		p.SendToPlayer("  [orange]Luck[] -[green] How much the universe tilts the odds in your favor.")
		p.SendToPlayer("Each of your 6 attributes starts at 10.  Each of the following 6 questions will allow you to raise an attribute by 2 points.  Each attribute can be raised no more than 4 points.  (For example, you could put two points into each stat, or put four points into just three of your stats, leaving the rest at 10.)")
		p.SendToPlayer(" ", "[darkblue]Press ENTER to continue...")
	case 1:
		p.SendToPlayer("You think back to your first real fight: a childhood bully cornered you in an alley.  As the large boy made known his intentions towards violence, how did you respond?")
		r.promptChoices(p, "I threw the first punchand scared him off. %s(+2 Strength)", "I nimbly dodged his blows until he was too tired to continue. %s(+2 Dexterity)", "I took his punches, one after the other, until he realized his efforts were in vain. %s(+2 Constitution)", "I used my charming wit to try to soothe the hostile beast. %s(+2 Charisma)", "I caused a burst of light from my fingers, blinding him long enough to escape. %s(+2 Magic)", "I called out to a passing guard who happened to be walking by. %s(+2 Luck)")
	case 2:
		p.SendToPlayer("Each spring, your village has a festival with many sports and activities.  What do you do to enjoy yourself?")
		r.promptChoices(p, "Compete in the arm wrestling competition. %s(+2 Strength)", "Practice my pick-pocketing among the distracted crowds. %s(+2 Dexterity)", "Drink all challengers under the table. %s(+2 Constitution)", "Anything, so long as you bring a date. %s(+2 Charisma)", "Learn new spells from the traveling booksellers. %s(+2 Magic)", "Win some coin at cards. %s(+2 Luck)")
	case 3:
		p.SendToPlayer("The saddest day of your childhood was watching helpless as your dog drowned in the river.  On that day you regretted:")
		r.promptChoices(p, "Not being a stronger swimmer. %s(+2 Strength)", "Not being able to throw a lasso. %s(+2 Dexterity)", "Not being able to hold your breath longer. %s(+2 Constitution)", "Not having anyone to help. %s(+2 Charisma)", "Not knowing a spell useful to the situation. %s(+2 Magic)", "Having such rotten luck. %s(+2 Luck)")
	case 4:
		p.SendToPlayer("A horse-drawn wagon is out of control, and there's a terrified child on board.  How do you save her?")
		r.promptChoices(p, "Grab the reigns and control the beast. %s(+2 Strength)", "Leap into the wagon and lift the child to safety. %s(+2 Dexterity)", "Step in front of the wagon, blocking its path with your body. %s(+2 Constitution)", "Calm the horse with your soothing words. %s(+2 Charisma)", "Casting a calming spell upon the beast. %s(+2 Magic)", "As you're about to act, the horse breaks free and the wagon rolls to a stop. %s(+2 Luck)")
	case 5:
		p.SendToPlayer("When you were younger, your mother would call you into the kitchen for what reason?")
		r.promptChoices(p, "To lift something heavy. %s(+2 Strength)", "To reach something up high. %s(+2 Dexterity)", "To see if the milk had gone bad.d %s(+2 Constitution)", "To keep her company. %s(+2 Charisma)", "To kindle the stove fire. %s(+2 Magic)", "To find something tiny she had dropped. %s(+2 Luck)")
	case 6:
		p.SendToPlayer("Where do you hide your money?")
		r.promptChoices(p, "Under a heavy stone. %s(+2 Strength)", "Up in the rafters. %s(+2 Dexterity)", "Beneath the garbage pile. %s(+2 Constitution)", "People don't charge you when you're this handsome! %s(+2 Charisma)", "In a box with no lid. %s(+2 Magic)", "You just seem to always find what you need. %s(+2 Luck)")
	}
}

func (r *RoomNewCharStats) promptChoices(p intf.Player, c1, c2, c3, c4, c5, c6 string) {
	if p == nil || p.GetChar() == nil {
		return
	}
	choice := 1
	if p.GetChar().GetStr() < 14 {
		p.SendToPlayer("  [darkblue]" + strconv.Itoa(choice) + ") " + strings.Replace(c1, "%s", "[salmon]", -1))
		choice++
	}
	if p.GetChar().GetDex() < 14 {
		p.SendToPlayer("  [darkblue]" + strconv.Itoa(choice) + ") " + strings.Replace(c2, "%s", "[salmon]", -1))
		choice++
	}
	if p.GetChar().GetCon() < 14 {
		p.SendToPlayer("  [darkblue]" + strconv.Itoa(choice) + ") " + strings.Replace(c3, "%s", "[salmon]", -1))
		choice++
	}
	if p.GetChar().GetCha() < 14 {
		p.SendToPlayer("  [darkblue]" + strconv.Itoa(choice) + ") " + strings.Replace(c4, "%s", "[salmon]", -1))
		choice++
	}
	if p.GetChar().GetMag() < 14 {
		p.SendToPlayer("  [darkblue]" + strconv.Itoa(choice) + ") " + strings.Replace(c5, "%s", "[salmon]", -1))
		choice++
	}
	if p.GetChar().GetLuc() < 14 {
		p.SendToPlayer("  [darkblue]" + strconv.Itoa(choice) + ") " + strings.Replace(c6, "%s", "[salmon]", -1))
	}
}

func (r *RoomNewCharStats) incStat(p intf.Player, v string) {
	if p == nil || p.GetChar() == nil {
		return
	}
	i, _ := strconv.Atoi(v)
	idx := 1
	if p.GetChar().GetStr() < 14 {
		if idx == i {
			p.GetChar().SetStr(p.GetChar().GetStr() + 2)
			return
		}
		idx++
	}
	if p.GetChar().GetDex() < 14 {
		if idx == i {
			p.GetChar().SetDex(p.GetChar().GetDex() + 2)
			return
		}
		idx++
	}
	if idx == i {
		p.GetChar().SetCon(p.GetChar().GetCon() + 2)
		return
	} else if p.GetChar().GetCon() < 14 {
		idx++
	}
	if p.GetChar().GetCha() < 14 {
		if idx == i {
			p.GetChar().SetCha(p.GetChar().GetCha() + 2)
			return
		}
		idx++
	}
	if p.GetChar().GetMag() < 14 {
		if idx == i {
			p.GetChar().SetMag(p.GetChar().GetMag() + 2)
			return
		}
	}
	p.GetChar().SetLuc(p.GetChar().GetLuc() + 2)
}
