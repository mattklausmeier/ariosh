package rooms

import (
	"io/ioutil"
	"strconv"
	"strings"

	"bitbucket.org/mattklausmeier/ariosh/ids/itemid"

	"bitbucket.org/mattklausmeier/ariosh/dataio"
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomLoginPassword handles the password portion of the user login process
type RoomLoginPassword struct {
	BaseRoom
}

// TakePlayer adds a player to this room
func (r *RoomLoginPassword) TakePlayer(p intf.Player) {
	if p == nil {
		return
	}
	r.players = append(r.players, p)
	p.SetRoom(r)
	p.SendToPlayer("[pass]Please enter your password:")
}

// HandleInput handles user input while in this room
func (r *RoomLoginPassword) HandleInput(p intf.Player, str string) bool {
	if p == nil || p.GetGame() == nil || p.GetChar() == nil {
		return false
	}
	if dataio.PasswordCorrect(p.GetUsername(), str) {
		p.SendToPlayer(" ", "Use [command][b]help[][] at any time if you need it.")
		content, err := ioutil.ReadFile(dataio.CharnameFile(p.GetUsername()))
		if err != nil {
			p.SendToPlayer("[inv]")
			p.GetGame().MovePlayerToRoom(p, roomid.NewCharRace)
		} else {
			p.SetLoggedIn(true)
			p.SetChar(intf.Char{Name: string(content)})
			data := dataio.ReadChar(string(content))
			strs := strings.Split(data, "\n")
			if len(strs) > 0 {
				p.GetChar().SetClassString(strs[0])
			}
			if len(strs) > 1 {
				p.GetChar().SetClassString(strs[1])
			}
			if len(strs) > 2 {
				p.GetChar().SetGenderString(strs[2])
			}
			if len(strs) > 3 {
				p.GetChar().SetStats(strs[3])
			}
			if len(strs) > 4 {
				r.parseInv(strs[4], p)
				p.GetChar().Inv(p)
			}
			if len(strs) > 5 {
				p.GetGame().MovePlayerToRoom(p, roomid.AtoRT(strs[5]))
			} else {
				p.GetGame().MovePlayerToRoom(p, roomid.WhitehallFountain)
			}
		}
	} else {
		p.SendToPlayer(" ", "[ff0000]Sorry, that password is incorrect!")
		p.SendToPlayer("[pass]Please enter your password:")
	}
	return true
}

func (r *RoomLoginPassword) parseInv(i string, p intf.Player) {
	if p == nil || p.GetChar() == nil {
		return
	}
	for a := 0; a < len(i); a++ {
		i = i[a:]
		idx := strings.Index(i, "{")
		if idx == -1 {
			break
		}
		idx2 := strings.Index(i, "}")
		if idx2 == -1 {
			break
		}
		if idx2-idx < 1 {
			break
		}
		id, err := strconv.Atoi(i[0:idx])
		if err != nil {
			break
		}
		data := i[idx+1 : idx2]
		item := r.Factory.Item(itemid.ItemType(id))
		item.Unmarshall(data)
		p.GetChar().TakeItem(item)
		a = idx2
	}
}
