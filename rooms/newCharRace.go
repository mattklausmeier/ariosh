package rooms

import (
	"strconv"
	"strings"

	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomNewCharRace is where the player chooses their character's race
type RoomNewCharRace struct {
	BaseRoom
	raceChoices map[intf.Player]int
}

// TakePlayer adds a player to this room
func (r *RoomNewCharRace) TakePlayer(p intf.Player) {
	if p == nil {
		return
	}
	r.BaseRoom.TakePlayer(p, r)
	if r.raceChoices == nil {
		r.raceChoices = make(map[intf.Player]int)
	}
	r.prompt(p)
}

// LosePlayer removes the player from raceChoices map before calling the BaseRoom's LosePlayer
func (r *RoomNewCharRace) LosePlayer(p intf.Player) {
	if p == nil {
		return
	}
	if r.raceChoices != nil {
		delete(r.raceChoices, p)
	}
	r.BaseRoom.LosePlayer(p)
}

func (r *RoomNewCharRace) prompt(p intf.Player) {
	if p == nil {
		return
	}
	p.SendToPlayer(" ", "[title][blue]Choose Your Character's Race")
	p.SendToPlayer("Late at night, as you calm your mind in preparation for sleep, you can still recall the sensation of being lovingly cradled in your father's arms.  What do your tiny hands feel as they reach out for his face?")
	p.SendToPlayer("  [darkblue]1) A mighty, braided beard and a thick nose. (Dwarf or Half-Dwarf)")
	p.SendToPlayer("  [darkblue]2) Long, pointed ears and smooth chin. (Elf or Half-Elf)")
	p.SendToPlayer("  [darkblue]3) Greenish skin and fierceome tusks. (Orc or Half-Orc)")
	p.SendToPlayer("  [darkblue]4) Scaley skin and eyes with pupil slits. (Drakkin)")
	p.SendToPlayer("  [darkblue]5) Round, pink ears and a scratchy beard. (Human)")
	p.SendToPlayer("  [darkblue]6) Fur and whiskers? (Fokhai)")
}

// HandleInput handles user input while in this room
func (r *RoomNewCharRace) HandleInput(p intf.Player, str string) bool {
	if p == nil || p.GetChar() == nil || p.GetGame() == nil {
		return false
	}
	if r.raceChoices[p] == 0 {
		trimmed := strings.TrimSpace(str)
		switch trimmed {
		case "1":
			r.raceChoices[p] = 1
			p.SendToPlayer(" ", "Did the other children tease you for being tall?")
			p.SendToPlayer("  [darkblue]1) Yes, my mother was a human. (Half-Dwarf)")
			p.SendToPlayer("  [darkblue]2) No, I'm pure Dwarf, through and through! (Dwarf)")
			return true
		case "2":
			r.raceChoices[p] = 2
			p.SendToPlayer(" ", "Did the other children tease you for having rounded ears?")
			p.SendToPlayer("  [darkblue]1) Yes, my mother was a human. (Half-Elf)")
			p.SendToPlayer("  [darkblue]2) No, I'm pure Elf, through and through! (Elf)")
			return true
		case "3":
			r.raceChoices[p] = 3
			p.SendToPlayer(" ", "Did the other children tease you for having pink skin")
			p.SendToPlayer("  [darkblue]1) Yes, my mother was a human. (Half-Orc)")
			p.SendToPlayer("  [darkblue]2) No, I'm pure Orc, through and through! (Orc)")
			return true
		case "4":
			p.GetChar().SetRace(intf.Drakkin)
		case "5":
			p.GetChar().SetRace(intf.Human)
		case "6":
			p.GetChar().SetRace(intf.Fokhai)
		default:
			p.SendToPlayer(" ", "[title][red][b]Please choose a number 1-6")
			r.prompt(p)
			return true
		}
		p.SendToPlayer(" ", "[title][blue]You are "+r.article(p.GetChar().GetRaceString())+" [salmon]"+p.GetChar().GetRaceString())
		p.GetGame().MovePlayerToRoom(p, roomid.NewCharClass)
		return true
	}
	trimmed := strings.TrimSpace(str)
	if trimmed == "1" || trimmed == "2" {
		switch r.raceChoices[p] {
		case 1:
			if trimmed == "1" {
				p.GetChar().SetRace(intf.HalfDwarf)
			} else {
				p.GetChar().SetRace(intf.Dwarf)
			}
		case 2:
			if trimmed == "1" {
				p.GetChar().SetRace(intf.HalfElf)
			} else {
				p.GetChar().SetRace(intf.Elf)
			}
		case 3:
			if trimmed == "1" {
				p.GetChar().SetRace(intf.HalfOrc)
			} else {
				p.GetChar().SetRace(intf.Orc)
			}
		}
		p.SendToPlayer(" ", "[title][blue]You are "+r.article(p.GetChar().GetRaceString())+" [salmon]"+p.GetChar().GetRaceString())
		p.GetGame().MovePlayerToRoom(p, roomid.NewCharClass)
		return true
	}
	var choice = strconv.Itoa(r.raceChoices[p])
	p.SendToPlayer(" ", "[title][red][b]Please choose a number 1 or 2")
	r.raceChoices[p] = 0
	return r.HandleInput(p, choice)
}

func (r *RoomNewCharRace) article(s string) string {
	if len(s) == 0 {
		return ""
	}
	if strings.ToLower(s[0:1]) == "a" || strings.ToLower(s[0:1]) == "e" || strings.ToLower(s[0:1]) == "i" ||
		strings.ToLower(s[0:1]) == "o" || strings.ToLower(s[0:1]) == "u" {
		return "an"
	}
	return "a"
}
