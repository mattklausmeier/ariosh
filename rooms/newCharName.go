package rooms

import (
	"strings"

	"bitbucket.org/mattklausmeier/ariosh/dataio"
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomNewCharName handles the creation of a new character
type RoomNewCharName struct {
	BaseRoom
	names map[intf.Player]string
}

// TakePlayer adds a player to this room
func (r *RoomNewCharName) TakePlayer(p intf.Player) {
	if p == nil {
		return
	}
	if r.names == nil {
		r.names = make(map[intf.Player]string)
	}
	r.players = append(r.players, p)
	p.SetRoom(r)
	r.prompt(p)
}

// LosePlayer removes the player from name map before calling the BaseRoom's LosePlayer
func (r *RoomNewCharName) LosePlayer(p intf.Player) {
	if p == nil {
		return
	}
	if r.names != nil {
		delete(r.names, p)
	}
	r.BaseRoom.LosePlayer(p)
}

func (r *RoomNewCharName) prompt(p intf.Player) {
	if p == nil {
		return
	}
	p.SendToPlayer(" ", "[blue][title]It's time to choose your character's name!  Here are the guidelines:")
	p.SendToPlayer("* Must be at least 2 characters long")
	p.SendToPlayer("* Must be no more than 20 characters long")
	p.SendToPlayer("* Can only contain letters, spaces, and punctuation")
	p.SendToPlayer("* Valid punctuation: period, comma, dash, single quote, double quote")
	p.SendToPlayer("* Leading/trailing spaces will be ignored")
	p.SendToPlayer("* Must not be the name of an existing character")
	p.SendToPlayer("* No double spaces")
	p.SendToPlayer("* Should be something fantasy sounding and not easily recognizable (No \"Trump\" or \"Frodo\")")
	p.SendToPlayer("* Must not be confusingly similar to another player's name.")
}

// HandleInput handles user input while in this room
func (r *RoomNewCharName) HandleInput(p intf.Player, str string) bool {
	if p == nil || p.GetGame() == nil || p.GetChar() == nil {
		return false
	}
	trimmed := strings.TrimSpace(str)
	if name, ok := r.names[p]; ok {
		if strings.ToLower(trimmed) == "y" || strings.ToLower(trimmed) == "yes" {
			dataio.CreateCharDirectory(p.GetUsername(), r.names[p])
			p.SetChar(intf.Char{Name: r.names[p]})
			p.SendToPlayer(" ", "[title][blue]You are named [salmon]"+p.GetChar().Name)
			p.GetGame().SaveChar(p)
			delete(r.names, p)
			p.SetLoggedIn(true)
			p.GetGame().MovePlayerToRoom(p, roomid.WhitehallFountain)
		} else if strings.ToLower(trimmed) == "n" || strings.ToLower(trimmed) == "no" {
			delete(r.names, p)
			r.prompt(p)
		} else {
			p.SendToPlayer(" ", "[title][red][b]I need a '[command][b]yes[][]' or '[command][b]no[][]' answer.")
			p.SendToPlayer(" ", "[title]Would you like to use the name '"+name+"'? [command][b][y/n][][]")
		}
	} else {
		p.SendToPlayer(" ")
		switch r.validateCharacterName(trimmed) {
		case 0:
			r.names[p] = trimmed
			p.SendToPlayer(" ", "[title][salmon][b]"+trimmed+"[][] is valid and available!")
			p.SendToPlayer("Would you like to use that name? [command][b][y/n][][]")
		case 1:
			p.SendToPlayer(" ", "[title][red]Sorry, you've used an invalid character.")
			r.prompt(p)
		case 2:
			p.SendToPlayer(" ", "[title][red]Sorry, no double spaces allowed.")
			r.prompt(p)
		case 3:
			p.SendToPlayer(" ", "[title][red]Sorry, that character name is taken.")
			r.prompt(p)
		case 4:
			p.SendToPlayer(" ", "[title][red]Sorry, that name is too short.")
			r.prompt(p)
		case 5:
			p.SendToPlayer(" ", "[title][red]Sorry, that name is too long.")
			r.prompt(p)
		}
	}
	return true
}

func (r *RoomNewCharName) validateCharacterName(char string) int {
	validChars := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz .,-'\""
	for a := 0; a < len(char); a++ {
		if !strings.ContainsRune(validChars, rune(char[a])) {
			return 1
		}
	}
	if strings.Contains(char, "  ") {
		return 2
	}
	if dataio.CharDirExists(char) {
		return 3
	}
	if len(char) < 2 {
		return 4
	}
	if len(char) > 20 {
		return 5
	}
	return 0
}
