package rooms

import (
	"strings"

	"bitbucket.org/mattklausmeier/ariosh/dataio"
	"bitbucket.org/mattklausmeier/ariosh/ids/roomid"
	"bitbucket.org/mattklausmeier/ariosh/intf"
)

// RoomNewUser handles creating a new user
type RoomNewUser struct {
	BaseRoom
}

// TakePlayer adds a player to this room
func (r *RoomNewUser) TakePlayer(p intf.Player) {
	if p == nil {
		return
	}
	r.players = append(r.players, p)
	p.SetRoom(r)
	p.SendToPlayer(" ", "[title]Please select the username you'd like to login with:")
	p.SendToPlayer(" - 20 characters max")
	p.SendToPlayer(" - Your username will be invisible to other users")
	p.SendToPlayer(" - You will select a different name for your character later")
}

// LosePlayer removes a player from this room
func (r *RoomNewUser) LosePlayer(p intf.Player) {
	if p == nil {
		return
	}
	if p.GetUsername() != "" && !dataio.UserHasPassword(p.GetUsername()) {
		dataio.DeleteUserDirectory(p.GetUsername())
	}
	r.players = intf.RemovePlayerFromSlice(p, r.players)
}

func isKeyword(str string) bool {
	array := [...]string{"admin", "new", "who"}
	for a := 0; a < len(array); a++ {
		if array[a] == strings.ToLower(str) {
			return true
		}
	}
	return false
}

// HandleInput handles user input while in this room
func (r *RoomNewUser) HandleInput(p intf.Player, str string) bool {
	if p == nil {
		return false
	}
	if p.GetUsername() == "" {
		// Check existing name
		trim := strings.TrimSpace(str)
		p.SendToPlayer(" ")
		if len(trim) > 20 {
			p.SendToPlayer(trim)
			p.SendToPlayer("[red]Sorry, that username is too long.")
			p.SendToPlayer("Please enter the username you would like (20 characters max):")
		} else if len(trim) < 2 {
			p.SendToPlayer(trim)
			p.SendToPlayer("[red]Sorry, that username is too short.")
			p.SendToPlayer("Please enter the username you would like (20 characters max):")
		} else if dataio.UserDirExists(trim) {
			p.SendToPlayer("[red]Sorry, that username is taken.")
			p.SendToPlayer("Please enter the username you would like (20 characters max):")
		} else if isKeyword(trim) {
			p.SendToPlayer("[red]Sorry, that username is reserved.")
			p.SendToPlayer("Please enter the username you would like (20 characters max):")
		} else {
			dataio.CreateUserDirectory(trim)
			p.SetUsername(trim)
			p.SendToPlayer("[green]Username accepted![]")
			p.SendToPlayer("[pass]Please enter the password you'd like to use:")
		}
	} else if dataio.UserHasPassword(p.GetUsername()) {
		// Verify password
		if dataio.PasswordCorrect(p.GetUsername(), str) {
			p.SendToPlayer(" ", "[green][b]Password Accepted!")
			p.SendToPlayer("[hr]", "[center]Welcome to [salmon][b]Ariosh Online!", "[hr]")
			p.SendToPlayer("[inv]")
			p.GetGame().MovePlayerToRoom(p, roomid.NewCharRace)
		} else {
			dataio.DeletePassword(p.GetUsername())
			p.SendToPlayer(" ", "[red]Sorry, those passwords don't match!")
			p.SendToPlayer("[pass]Please enter the password you'd like to use:")
		}
	} else {
		// Record password
		dataio.SavePassword(p.GetUsername(), str)
		p.SendToPlayer(" ", "[pass]Please enter your password again to verify:")
	}
	return true
}
