package main

import (
	"log"
	"os"
	"os/signal"

	"bitbucket.org/mattklausmeier/ariosh/factory"

	"bitbucket.org/mattklausmeier/ariosh/game"
	"bitbucket.org/mattklausmeier/ariosh/server"

	"golang.org/x/net/websocket"
)

var mainGame = game.Game{Factory: &factory.Factory{}}

func main() {
	mainGame.Poke()
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	go func() {
		<-c
		mainGame.CrashBackup()
		os.Exit(1)
	}()
	log.Fatal(server.Run(echoServer))
}

func echoServer(ws *websocket.Conn) {
	player := mainGame.AddPlayer(ws)
	defer func() {
		mainGame.RemovePlayer(player)
	}()
	for {
		var err error
		var input string
		if err = websocket.Message.Receive(ws, &input); err != nil {
			break
		}
		player.HandlePlayerInput(input)
		if player.DidSendFail() {
			break
		}
	}
}
